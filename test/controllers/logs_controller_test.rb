require 'test_helper'

class LogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @log = logs(:one)
  end

  test "should get index" do
    get logs_url
    assert_response :success
  end

  test "should get new" do
    get new_log_url
    assert_response :success
  end

  test "should create log" do
    assert_difference('Log.count') do
      post logs_url, params: { log: { channel: @log.channel, elevation: @log.elevation, field_10: @log.field_10, field_1: @log.field_1, field_2: @log.field_2, field_3: @log.field_3, field_4: @log.field_4, field_5: @log.field_5, field_6: @log.field_6, field_7: @log.field_7, field_8: @log.field_8, field_9: @log.field_9, latitude: @log.latitude, longitude: @log.longitude } }
    end

    assert_redirected_to log_url(Log.last)
  end

  test "should show log" do
    get log_url(@log)
    assert_response :success
  end

  test "should get edit" do
    get edit_log_url(@log)
    assert_response :success
  end

  test "should update log" do
    patch log_url(@log), params: { log: { channel: @log.channel, elevation: @log.elevation, field_10: @log.field_10, field_1: @log.field_1, field_2: @log.field_2, field_3: @log.field_3, field_4: @log.field_4, field_5: @log.field_5, field_6: @log.field_6, field_7: @log.field_7, field_8: @log.field_8, field_9: @log.field_9, latitude: @log.latitude, longitude: @log.longitude } }
    assert_redirected_to log_url(@log)
  end

  test "should destroy log" do
    assert_difference('Log.count', -1) do
      delete log_url(@log)
    end

    assert_redirected_to logs_url
  end
end
