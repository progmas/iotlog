require 'test_helper'

class AppControllerTest < ActionDispatch::IntegrationTest
  test "should get read" do
    get app_read_url
    assert_response :success
  end

  test "should get write" do
    get app_write_url
    assert_response :success
  end

end
