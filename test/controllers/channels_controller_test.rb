require 'test_helper'

class ChannelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @channel = channels(:one)
  end

  test "should get index" do
    get channels_url
    assert_response :success
  end

  test "should get new" do
    get new_channel_url
    assert_response :success
  end

  test "should create channel" do
    assert_difference('Channel.count') do
      post channels_url, params: { channel: { access: @channel.access, active: @channel.active, description: @channel.description, elevation: @channel.elevation, field_10: @channel.field_10, field_1: @channel.field_1, field_2: @channel.field_2, field_3: @channel.field_3, field_4: @channel.field_4, field_5: @channel.field_5, field_6: @channel.field_6, field_7: @channel.field_7, field_8: @channel.field_8, field_9: @channel.field_9, latitude: @channel.latitude, longitude: @channel.longitude, metadata: @channel.metadata, name: @channel.name, read_api_key: @channel.read_api_key, tags: @channel.tags, user_id: @channel.user_id, write_api_key: @channel.write_api_key } }
    end

    assert_redirected_to channel_url(Channel.last)
  end

  test "should show channel" do
    get channel_url(@channel)
    assert_response :success
  end

  test "should get edit" do
    get edit_channel_url(@channel)
    assert_response :success
  end

  test "should update channel" do
    patch channel_url(@channel), params: { channel: { access: @channel.access, active: @channel.active, description: @channel.description, elevation: @channel.elevation, field_10: @channel.field_10, field_1: @channel.field_1, field_2: @channel.field_2, field_3: @channel.field_3, field_4: @channel.field_4, field_5: @channel.field_5, field_6: @channel.field_6, field_7: @channel.field_7, field_8: @channel.field_8, field_9: @channel.field_9, latitude: @channel.latitude, longitude: @channel.longitude, metadata: @channel.metadata, name: @channel.name, read_api_key: @channel.read_api_key, tags: @channel.tags, user_id: @channel.user_id, write_api_key: @channel.write_api_key } }
    assert_redirected_to channel_url(@channel)
  end

  test "should destroy channel" do
    assert_difference('Channel.count', -1) do
      delete channel_url(@channel)
    end

    assert_redirected_to channels_url
  end
end
