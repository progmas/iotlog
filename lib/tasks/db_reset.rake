namespace :vt do
  desc 'Veritabanını sıfırlar'
  task :reset, [] => :environment do
    raise 'production''da çalıştırmazsın.' if Rails.env.production?

    Rake::Task['db:drop'].execute
    Rake::Task['db:create'].execute
    Rake::Task['db:migrate'].execute
    Rake::Task['db:seed'].execute
  end
end