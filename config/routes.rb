Rails.application.routes.draw do
  get 'graphic/index'

  get 'report/index'

  resources :fields
  resources :events
  resources :event_details
  resources :logs
  match 'app/read', to: 'app#read', as: :read_path, via: [:get, :post]
  match 'app/get_channel', to: 'app#get_channel', as: :get_channel, via: [:get, :post]
  match 'app/write', to: 'app#write', as: :write_path, via: [:get, :post]
  match 'app/get_channel_info', to: 'app#get_channel_info', as: :get_channel_info_path, via: [:get, :post]

  resources :channels
  resources :reports
  get 'home/index', as: :index

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  # Push Notification
  post '/subscribe' => 'subscriptions#create'
  post '/unsubscribe' => 'subscriptions#destroy'

  authenticate :user do
    mount Blazer::Engine, at: "blazer"
  end
end
