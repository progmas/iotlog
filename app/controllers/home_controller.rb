class HomeController < ApplicationController
  before_action :authenticate_user!

  protect_from_forgery with: :exception
  def index
    # @fields = Channel.joins(:fields).where(access: :public_access).select('channels.name', :description)
    @public_channels = Channel.select('channels.name', :description)
                           .where(access: :public_access)
  end
end
