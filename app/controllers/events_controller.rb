class EventsController < ApplicationController
  before_action :authenticate_user!

  protect_from_forgery with: :exception
  before_action :set_event, only: [:edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index
    @events = current_user.events.includes(:event_details, :sent_sms_messages, :sent_emails, :sent_callbacks, :sent_push_notifications)
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.includes(:event_details, :sent_sms_messages, :sent_emails, :sent_callbacks, :sent_push_notifications).find(params[:id])
  end

  # GET /events/new
  def new
    @event = current_user.events.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = current_user.events.new(event_params)

    respond_to do |format|
      if @event.save
        FcmTopicsService.new("event_#{@event.id}", session[:subscriber]).create
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:user_id, :script, :message, :sms, :email, :push_notification, :call_back_url,
                                    event_details_attributes: [:id, :field_id, :operation, :value, :_destroy],
                                    sent_sms_messages_attributes: [:id, :phone, :_destroy],
                                    sent_emails_attributes: [:id, :email, :_destroy],
                                    sent_callbacks_attributes: [:id, :callback, :_destroy],
                                    sent_push_notifications_attributes: [:id, :push_notification, :_destroy],
                                    )
    end
end
