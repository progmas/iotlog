class AppController < ApplicationController
  before_action :verify_authenticity_token, :except => [:write, :read, :get_channel, :get_channel_info]

  def read
    channel = Channel.find_by_read_api_key(params[:api_key])
    if channel.blank?
      render json: {state: false, message: 'Not found channel'}
      return false
    end

    if channel.present?
      field = params[:field].strip
      count = params[:count].present? ? params[:count].to_i : 1
      result = Log.joins(:field).where('fields.name = ?', field)
                   .last(count)
                   .pluck(:value)
      render json: result
    end
  end

  def write
    begin
      channel = Channel.find_by_write_api_key(params[:api_key])
      if channel.blank?
        render json: {state: false, message: 'Not found channel'}
        return false
      end

      unless channel.active?
        render json: {state: false, message: 'This channel isn''t active'}
        return false
      end

      # method = channel.send_method
      # if method.present? && method != :any && method != request.request_method
      #   render json: {state: false, message: 'You don''t have send data with this method'}
      #   return false
      # end

      fields = params.except(:api_key, :controller, :action).keys
      fields.each do |field|
        db_field = Field.where(channel: channel, name: field.parameterize).first
        if db_field.present?
          log = MongoLog.new
          log.field_id = db_field.id
          log.value = params[field]
          log.ip_address = request.ip
          log.created_at = Time.current
          log.save

          SentFirebaseWorker.perform_async db_field.id, params[field], request.ip
          EventControlWorker.perform_async db_field.id
        end
      end

      render json: {state: true}
    rescue
      render json: {state: false}
    end
  end

  def get_channel
    channel = Channel.find_by_read_api_key(params[:api_key])
    if channel.blank?
      render json: {state: false, message: 'Not found channel'}
      return false
    end

    if channel.present?
      fields = params[:fields].split(',').map(&:strip)
      db_fields = Field.where(channel: channel, name: [fields])
      count = params[:count].present? ? params[:count].to_i : 1
      result = Hash.new
      db_fields.each do |db_field|
        result[db_field.name] = Log.where(field_id: db_field.id)
                                    .last(count)
                                    .pluck(:value)

      end

      render json: result
    end
  end

  def get_channel_info
    channel = Channel.select(:name, :description, :metadata, :tags, :latitude, :longitude, :elevation, :access, :send_method, :active)
                  .find_by_read_api_key(params[:api_key]).to_json
    if channel.blank?
      render json: {state: false, message: 'Not found channel'}
      return false
    else
      render json: channel
      return true
    end
  end
end
