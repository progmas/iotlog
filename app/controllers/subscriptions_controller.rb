class SubscriptionsController < ApplicationController

  def create
    session[:subscriber] = params[:subscriber]
    render json: :success
  end

  def destroy
    session.delete :subscriber
    render json: :success
  end

end
