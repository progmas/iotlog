class SendPushNotificationJob < ApplicationJob
  queue_as :default

  def perform(event_id)
    # Do something later
    event = Event.find(event_id)
    if event.present? && event.sent_push_notifications.present?

      event.sent_push_notifications.each do |notification|
        client = Pushbullet::Client.new(PUSHBULLET_API_KEY)
        client.push_note_to(notification.push_notification, 'IoT Log Management', event.message)
        puts "---------------------->#{notification.push_notification} ye notification gönderildi"
      end
    end
  end
end
