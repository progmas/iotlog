class EventControlJob < ApplicationJob
  queue_as :default

  def perform(field_id, value)
    @events = Event.joins(:event_details)
                  .includes(:event_details)
                  .where('event_details.field_id = ? ', field_id)

    # @events = Event.left_joins(:event_details, :event_script_fields)
    #               .includes(:event_details)
    #               .where('event_script_fields.field_id = ? OR event_details.field_id = ? ', field_id, field_id)
    @events.each do |event|
      result = true
      event.event_details.each do |event_detail|
        puts "==>#{event_detail.field.name} - #{event_detail.field_id} #{event_detail.operation} #{event_detail.value}"
        result &&= eval("#{value.to_f}#{event_detail.operation}#{event_detail.value}")
      end

      # Sonuc true ise gerekli geri bildirimleri ver
      if result
        if event.sent_sms_messages.present?
          SendSmsJob.perform_now(event.id)
        end

        if event.sent_emails.present?
          SendEmailJob.perform_now(event.id)
        end

        if event.sent_callbacks.present?
          SendToCallbackJob.perform_now(event.id)
        end

        if event.sent_push_notifications.present?
          SendPushNotificationJob.perform_now(event.id)
        end
      end
    end

    @events = Event.joins(:event_script_fields)
                  .includes(:event_script_fields)
                  .where('event_script_fields.field_id = ?', field_id)
    @events.each do |event|
      result = true
      puts "==>#{event.script}"
      result &&= eval(event.script)


      # Sonuc true ise gerekli geri bildirimleri ver
      if result
        if event.sent_sms_messages.present?
          SendSmsJob.perform_now(event.id)
        end

        if event.sent_emails.present?
          SendEmailJob.perform_now(event.id)
        end

        if event.sent_callbacks.present?
          SendToCallbackJob.perform_now(event.id)
        end

        if event.sent_push_notifications.present?
          SendPushNotificationJob.perform_now(event.id)
        end
      end
    end
  end
end
