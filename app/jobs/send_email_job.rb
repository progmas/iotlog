class SendEmailJob < ApplicationJob
  queue_as :default

  def perform(event_id)
    # Do something later
    event = Event.find(event_id)
    if event.present? && event.sent_emails.present?
  		event.sent_emails.each do |email|
  			SendMailMailer.send_mail(email.email, 'IoT Log Management', event.message).deliver_later
  			puts "---------------------->#{email.email} ye email gönderildi"

        # init = FCM.new('AAAAuzKmm7w:APA91bEgCgUnd5oBkuhgKMoAKCOAYkgp2CB2D8bRA59gigEtWdn-XrCji3h_F-UsZh8oGCi08kfH02_ghYcAVFEZQnTA804I1DR9SYXIGaoLDGqUm9zHI8wKgUYUBDf1son664ZDO80f')
        # fcm.send_to_topic("event_1", notification:key => "value",
        #     {"title": 'Bir yeni bildirim.',
        #      "body": "body",
        #      "icon": '/assets/icons/icon-192x192.png',
        #      "click_action": 'localhost:3000'
        #     })
        #


        # init = FCM.new('cTAgG6QuO4k:APA91bFkVzMcwx6VK_RxWrjFdvACjTJBXenK386N5hQM8X9y-f8ggqpVot2AZ_F7bYQ3IVwpmmvGRvyAs6uYnEMXJS_whLThp9p9rNn7D59B3z2bwlm8aQkNfulB9UzuzIjjkpM-qADk')
        # fcm.send_to_topic("event_1", notification:
        #     {"title": 'Bir yeni bildirim.',
        #      "body": "body",
        #      "icon": '/assets/icons/icon-192x192.png',
        #      "click_action": 'localhost:3000'
        #     })
  		end
    end
  end
end