class LogTransferJob < ApplicationJob
  queue_as :default

  def perform(log)
    # Do something later
    Log.create!(field_id: log.field_id,
                value: log.value,
                ip_address: log.ip_address,
                created_at: log.created_at)
  end
end
