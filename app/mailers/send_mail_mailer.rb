class SendMailMailer < ApplicationMailer
	def send_mail(to, subject, message)
		mail(to: to,
		     subject: subject,
		     body: message)
	end
end
