class FcmTopicsService
  attr_accessor :topic
  attr_accessor :subscriber

  def initialize(topic, subscriber)
    @topic = topic
    @subscriber = subscriber
  end

  def create_topic
    url = "https://iid.googleapis.com/iid/v1/#{@subscriber}/rel/topics/#{@topic}"
    headers = {
        "Content-Type": 'application/json',
        "Authorization": "key=AAAAuzKmm7w:APA91bEgCgUnd5oBkuhgKMoAKCOAYkgp2CB2D8bRA59gigEtWdn-XrCji3h_F-UsZh8oGCi08kfH02_ghYcAVFEZQnTA804I1DR9SYXIGaoLDGqUm9zHI8wKgUYUBDf1son664ZDO80f"
    }
    response = RestClient::Request.execute(
        method: :post,
        url: url,
        headers: headers
    )
  rescue RestClient::ExceptionWithResponse => e
    e.response
  end

  alias create create_topic
end