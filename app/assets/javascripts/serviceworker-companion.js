// [START get_messaging_object]
// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
// [END get_messaging_object]

if ('serviceWorker' in navigator) {
    service_worker();
}

function service_worker() {
    navigator.serviceWorker.register('serviceworker.js', { scope: './' })
        .then(function(registration) {
            console.log('Successfully registered!', ':^)', registration);
            // [START_REQUEST_PERMISSION]
            requestPermission();
            // [END_REQUEST_PERMISSION]
            messaging.useServiceWorker(registration);
            messaging.getToken()
                .then(function(currentToken) {
                    if (currentToken) {
                        sendTokenToServer(currentToken);
                        updateUIForPushEnabled(currentToken);
                    } else {
                        // Show permission request.
                        console.log('No Instance ID token available. Request permission to generate one.');

                        // Destroy active token
                        destroyToken();

                        // Show permission UI.
                        setTokenSentToServer(false);
                    }
                })
                .catch(function(err) {
                    console.log('An error occurred while retrieving token. ', err);
                    setTokenSentToServer(false);
                });
        }).catch(function(error) {
        console.log('Registration failed', ':^(', error);
    });
}

// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {
    messaging.getToken()
        .then(function(refreshedToken) {
            console.log('Token refreshed.');
            // Indicate that the new Instance ID token has not yet been sent to the
            // app server.
            setTokenSentToServer(false);
            // Send Instance ID token to app server.
            sendTokenToServer(refreshedToken);
            // [START_EXCLUDE]
            // Display new Instance ID token and clear UI of all previous messages.
            // [END_EXCLUDE]
        })
        .catch(function(err) {
            console.log('Unable to retrieve refreshed token ', err);
        });
});
// [END refresh_token]

// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a sevice worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function(payload) {
    console.log("Message received. ", payload);
    appendMessage(payload);
});
// [END receive_message]

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');
        // TODO(developer): Send the current token to your server.
        setTokenSentToServer(true);
    } else {
        console.log('Token already sent to server so won\'t send it again unless it changes');
    }

}

function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') == 1;
}

function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? 1 : 0);
}

function requestPermission() {
    console.log('Requesting permission...');
    // [START request_permission]
    messaging.requestPermission()
        .then(function() {
            console.log('Notification permission granted.');
        })
        .catch(function(err) {
            console.log('Unable to get permission to notify.', err);
        });
    // [END request_permission]
}

function destroyToken() {
    $.post("/unsubscribe", function(resp) {
        if (resp == "success") { console.log('Token deleted.'); } else { console.log("Token can not delete now!"); }
    });
}

// Add a message to the messages element.
function appendMessage(payload) {
    var data = JSON.stringify(payload, null, 2);
    var title = payload.notification.title || "Yeni bir bildirim."
    var body = payload.notification.body || "IoT Kayıt Yönetimi'nden yeni bir bildirim aldınız."
    var notification = new Notification(title, {
        "title": title,
        "body": body,
        "icon": "/assets/icons/icon-192x192.png"
    });
}

function updateUIForPushEnabled(currentToken) {
    $.post("/subscribe", { subscriber: currentToken }, function(resp) {
        if (resp == "success") { console.log(currentToken); } else { console.log(resp); }
    })
}