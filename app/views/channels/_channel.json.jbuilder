json.extract! channel, :id, :user_id, :name, :description, :metadata, :tags, :latitude, :longitude, :elevation, :access, :active, :read_api_key, :write_api_key, :created_at, :updated_at
json.url channel_url(channel, format: :json)
