json.extract! log, :id, :channel, :field_1, :field_2, :field_3, :field_4, :field_5, :field_6, :field_7, :field_8, :field_9, :field_10, :latitude, :longitude, :elevation, :created_at, :updated_at
json.url log_url(log, format: :json)
