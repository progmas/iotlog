json.extract! field, :id, :channel_id, :name, :type, :created_at, :updated_at
json.url field_url(field, format: :json)
