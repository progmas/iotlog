json.extract! event, :id, :user_id, :script, :message, :sms, :email, :push_notification, :call_back_url, :created_at, :updated_at
json.url event_url(event, format: :json)
