json.extract! event_detail, :id, :channel_id, :field, :operation, :value, :created_at, :updated_at
json.url event_detail_url(event_detail, format: :json)
