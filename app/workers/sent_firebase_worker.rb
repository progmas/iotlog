class SentFirebaseWorker
  include Sidekiq::Worker

  def perform(field_id, value, request_ip)
    # Do something
    channel = Field.find(field_id).channel
    if channel.firebase_secret_key.present? && channel.firebase_url.present?
      firebase_url = channel.firebase_url
      firebase_secret = channel.firebase_secret_key
      firebase = Firebase::Client.new(firebase_url, firebase_secret)
      firebase.push("iotlog", {
          field_id: field_id,
          value: value,
          ip_address: request_ip,
          created_at: Time.current
      })
    end
  end
end
