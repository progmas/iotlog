# == Schema Information
#
# Table name: fields
#
#  id         :integer          not null, primary key
#  channel_id :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Field < ApplicationRecord
  belongs_to :channel
  has_many :field_values
  has_many :logs

  before_save do
    self.name = self.name.parameterize
  end

  # default_scope { left_joins(:logs) }

  scope :logs, -> { left_joins(:logs) }
  scope :query, ->(name) { where(name: name) }
  scope :date, ->(symbol, date) { where("logs.created_at #{symbol} '#{date}'")}
  scope :between_date, ->(date1, date2) { where(created_at: date1..date2)}
end
