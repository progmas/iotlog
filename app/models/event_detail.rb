# == Schema Information
#
# Table name: event_details
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  field_id   :integer
#  operation  :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EventDetail < ApplicationRecord
  belongs_to :event
  belongs_to :field
end
