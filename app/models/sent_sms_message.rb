# == Schema Information
#
# Table name: sent_sms_messages
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SentSmsMessage < ApplicationRecord
  belongs_to :event
end
