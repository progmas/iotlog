# == Schema Information
#
# Table name: logs
#
#  id         :integer          not null, primary key
#  field_id   :integer
#  value      :decimal(, )
#  ip_address :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Log < ApplicationRecord
  belongs_to :field

  after_save do
    EventControlJob.perform_later(field_id, value)
  end
end
