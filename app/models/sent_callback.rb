# == Schema Information
#
# Table name: sent_callbacks
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  callback   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SentCallback < ApplicationRecord
  belongs_to :event
end
