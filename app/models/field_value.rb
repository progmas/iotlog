# == Schema Information
#
# Table name: field_values
#
#  id         :integer          not null, primary key
#  field_id   :integer
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FieldValue < ApplicationRecord
  belongs_to :field
end
