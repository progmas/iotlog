# == Schema Information
#
# Table name: sent_push_notifications
#
#  id                :integer          not null, primary key
#  event_id          :integer
#  push_notification :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class SentPushNotification < ApplicationRecord
  belongs_to :event
end
