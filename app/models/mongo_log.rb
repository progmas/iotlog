class MongoLog
  include Mongoid::Document
  include GlobalID::Identification

  field :field_id, type: Integer
  field :value, type: Float
  field :ip_address, type: String
  field :created_at, type: DateTime

  # field :channel, type: Integer
  # field :field_1, type: String
  # field :field_2, type: String
  # field :field_3, type: String
  # field :field_4, type: String
  # field :field_5, type: String
  # field :field_6, type: String
  # field :field_7, type: String
  # field :field_8, type: String
  # field :field_9, type: String
  # field :field_10, type: String
  # field :latitude, type: String
  # field :longitude, type: String
  # field :elevation, type: String

  scope :get_log, ->(field_id, limit = 20) { where(field_id: field_id).order_by(created_at: 'desc').limit(limit) }
  scope :get_field_last_value, ->(field_id) { where(field_id: field_id).order_by(created_at: 'desc').limit(1) }

  # job'a gönder
  after_save {
    LogTransferJob.perform_later(self)
  }
end
