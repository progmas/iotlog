# == Schema Information
#
# Table name: sent_emails
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SentEmail < ApplicationRecord
  belongs_to :event
end
