# == Schema Information
#
# Table name: channels
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  name                :string
#  description         :text
#  metadata            :text
#  tags                :text
#  latitude            :string
#  longitude           :string
#  elevation           :string
#  firebase_url        :string
#  firebase_secret_key :string
#  access              :integer
#  send_method         :integer
#  active              :boolean
#  read_api_key        :string
#  write_api_key       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Channel < ApplicationRecord
  belongs_to :user, required: true

  enum access: [:public_access, :private_access]
  enum send_method: [:get, :post, :any]

  has_many :fields, inverse_of: :channel
  accepts_nested_attributes_for :fields, reject_if: :all_blank, allow_destroy: true

  validates_presence_of :name, :description, :read_api_key, :write_api_key

  before_validation :create_apis, on: :create

  def create_apis
    self.read_api_key = rand(36**40).to_s(36).upcase
    self.write_api_key = rand(36**40).to_s(36).upcase
  end
end
