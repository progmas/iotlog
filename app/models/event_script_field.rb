class EventScriptField < ApplicationRecord
  belongs_to :event
  belongs_to :field
end
