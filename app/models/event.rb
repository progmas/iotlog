# == Schema Information
#
# Table name: events
#
#  id                :integer          not null, primary key
#  name              :string
#  user_id           :integer
#  script            :text
#  message           :string
#  sms               :boolean
#  email             :boolean
#  push_notification :boolean
#  call_back_url     :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Event < ApplicationRecord
  belongs_to :user

  has_many :event_details, inverse_of: :event, dependent: :destroy
  accepts_nested_attributes_for :event_details, reject_if: :all_blank, allow_destroy: true

  has_many :sent_sms_messages, inverse_of: :event, dependent: :destroy
  accepts_nested_attributes_for :sent_sms_messages, reject_if: :all_blank, allow_destroy: true

  has_many :sent_emails, inverse_of: :event, dependent: :destroy
  accepts_nested_attributes_for :sent_emails, reject_if: :all_blank, allow_destroy: true

  has_many :sent_callbacks, inverse_of: :event, dependent: :destroy
  accepts_nested_attributes_for :sent_callbacks, reject_if: :all_blank, allow_destroy: true

    has_many :sent_push_notifications, inverse_of: :event, dependent: :destroy
  accepts_nested_attributes_for :sent_push_notifications, reject_if: :all_blank, allow_destroy: true

  has_many :event_script_fields

  before_validation do
    begin
      # value hariç diğer field lar seçiliyor.
      self.script.scan(/:\w+/).uniq.delete_if {|x| x == ':value' }.each do |field|
        if self.user.fields.where(name: field.gsub(':','')).count == 0
          errors[:script] << "Event script is not valid. Field name: #{field}"
        end
      end
    rescue

    end
  end

  after_save do
    self.event_script_fields.destroy_all
    self.script.scan(/:\w+/).uniq.delete_if {|x| x == ':value' }.each do |field|
      field.slice!(0)
      self.event_script_fields.create!(field_id: Field.joins(:channel).where('channels.user_id = ?', self.user.id).where('fields.name = ?', field).first.id)
    end
  end
end
