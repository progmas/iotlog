class CreateLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :logs do |t|
      t.references :field, foreign_key: true
      t.decimal :value
      t.string :ip_address

      t.timestamps
    end
  end
end
