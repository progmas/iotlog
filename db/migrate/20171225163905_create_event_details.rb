class CreateEventDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :event_details do |t|
      t.references :event, foreign_key: true
      t.references :field, foreign_key: true
      t.string :operation
      t.string :value

      t.timestamps
    end
  end
end
