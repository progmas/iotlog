class CreateSentEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :sent_emails do |t|
      t.references :event, foreign_key: true
      t.string :email

      t.timestamps
    end
  end
end
