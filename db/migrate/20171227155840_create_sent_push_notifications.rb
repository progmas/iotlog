class CreateSentPushNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :sent_push_notifications do |t|
      t.references :event, foreign_key: true
      t.string :push_notification

      t.timestamps
    end
  end
end
