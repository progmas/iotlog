class CreateFields < ActiveRecord::Migration[5.1]
  def change
    create_table :fields do |t|
      t.references :channel, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
