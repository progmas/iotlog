class CreateEventScriptFields < ActiveRecord::Migration[5.1]
  def change
    create_table :event_script_fields do |t|
      t.references :event, foreign_key: true
      t.references :field, foreign_key: true

      t.timestamps
    end
  end
end
