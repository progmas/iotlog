class CreateSentCallbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :sent_callbacks do |t|
      t.references :event, foreign_key: true
      t.string :callback

      t.timestamps
    end
  end
end
