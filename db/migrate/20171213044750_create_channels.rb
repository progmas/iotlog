class CreateChannels < ActiveRecord::Migration[5.1]
  def change
    create_table :channels do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.text :description
      t.text :metadata
      t.text :tags
      t.string :latitude
      t.string :longitude
      t.string :elevation
      t.string :firebase_url
      t.string :firebase_secret_key
      t.integer :access # Public, Private
      t.integer :send_method # GET, POST
      t.boolean :active
      t.string :read_api_key, index: {unique: true}
      t.string :write_api_key, index: {unique: true}

      t.timestamps
    end
  end
end
