class CreateSentSmsMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :sent_sms_messages do |t|
      t.references :event, foreign_key: true
      t.string :phone

      t.timestamps
    end
  end
end
