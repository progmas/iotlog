class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.references :user, foreign_key: true
      t.text :script
      t.string :message
      t.boolean :sms
      t.boolean :email
      t.boolean :push_notification
      t.string :call_back_url

      t.timestamps
    end
  end
end
