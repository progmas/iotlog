class CreateFieldValues < ActiveRecord::Migration[5.1]
  def change
    create_table :field_values do |t|
      t.references :field, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
